/*!
    @file       hal-2000.h
    @project    Hal-2000
    @brief      Part of the Hal-2000 scenography design, this is a MIDI-based
                controller designed to be a physical interface of Ableton, Millumin, etc.
                Produced by https://www.domes-studio.com/
    @version    0.1.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       01/03/2024

    Copyright (C) 2024 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef HAL_2000_HPP__
#define HAL_2000_HPP__

#include "MIDIUSB.h"

#include "mux.hpp"
#include "sensors.hpp"

#define DEBUG 0

#define UPDATE_FREQ 25
#define HOW_MANY_SENSORS 6

long lastUpdate = 0;


/*
   Needed for MIDIUSB lib

*/

 void noteOn(byte channel, byte pitch, byte velocity) {
   midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
   MidiUSB.sendMIDI(noteOn);
 }

 void noteOff(byte channel, byte pitch, byte velocity) {
   midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
   MidiUSB.sendMIDI(noteOff);
 }

 void controlChange(byte channel, byte control, byte value) {
   midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
   MidiUSB.sendMIDI(event);
 }


#endif /* HAL_2000 */
