/*!
    @file       mux.h
    @project    Hal-2000
    @brief      Part of the Hal-2000 scenography design, this is a MIDI-based
                controller designed to be a physical interface of Ableton, Millumin, etc.
                Produced by https://www.domes-studio.com/
    @version    0.1.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       01/03/2024

    Copyright (C) 2024 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef MUX_HPP__
#define MUX_HPP__

#include "Arduino.h"

#include <ResponsiveAnalogRead.h>

#define DEBUG_MUX__ 0

class Mux
{
  public:
    Mux(uint8_t how_many_sensors);

    void setup(const uint8_t analogPin, const uint8_t *pins);
    
    void update();
    void update(uint8_t sensor);

    void set(uint8_t sensor);

    bool hasChanged(uint8_t sensor);

    int *get();
    int get(uint8_t sensor);
    int getLast(uint8_t sensor);
    int getRaw(void);

  private:
    uint8_t _how_many_sensors;
    uint8_t _how_many_pins;
    
    uint8_t _analogPin;

    uint8_t *_pins;
    uint8_t *_state;

    int *_analogValues;
    int *_lastAnalogValues;

    ResponsiveAnalogRead **_responsive;
};


#endif /* MUX */
