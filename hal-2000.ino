/*!
    @file       hal-2000.ino
    @project    Hal-2000
    @brief      Part of the Hal-2000 scenography design, this is a MIDI-based
                controller designed to be a physical interface of Ableton, Millumin, etc.
                Produced by https://www.domes-studio.com/
    @version    0.1.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       01/03/2024

    Copyright (C) 2024 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "Hal-2000.hpp"

Mux mux(HOW_MANY_SENSORS);

void setup() {
  // init serial
  Serial.begin(115200);

  //while (!Serial) {
  //  ; // wait for serial port to connect. Needed for native USB
  //}

  // wait for serial
  delay(2000);

  // setup A-B-C pins for 4051-Mux
  const uint8_t mux_pins[] = { 2, 3, 4 };
  mux.setup(A0, mux_pins);
}

void loop() {
  // update state of sensors
  if (millis() - lastUpdate > UPDATE_FREQ)
  {
    //
    mux.update();

    //
    int *sensorVal = mux.get();

    for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++)
    {
      int delta = sensorVal[sensor] - mux.getLast(sensor);

      // MIDI CC 3 :: undefined
      controlChange(sensor, 3, int ((float) sensorVal[sensor] / 1023 * 127));

      // front montant
      if (delta > 0 && abs(delta) > jump[inc][sensor])
      {
        // MIDI CC 9 :: undefined
        controlChange(sensor, 9, 127);

        // MIDI NOTE-ON VELOCITY 127
        noteOn(1, sensor + 1, 127);

        //
        // Serial.print("UP -> ");
        // Serial.print(sensor);
        // Serial.print(" :: ");
        // Serial.println(sensorVal[sensor]);
      }

      // front descendant
      else if (delta < 0 && abs(delta) > jump[dec][sensor])
      {
        // MIDI CC 9 :: undefined
        controlChange(sensor, 9, 0);

        // MIDI NOTE-OFF VELOCITY 0
        noteOff(1, sensor + 1, 0);

        //
        // Serial.print("DOWN -> ");
        // Serial.print(sensor);
        // Serial.print(" :: ");
        // Serial.println(sensorVal[sensor]);
      }

      //
      else {};

      //
      MidiUSB.flush();
    }
  }




  /* ------ DRAFT HERE ------ */




  // if (DEBUG == 2) {
  //   for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++) {
  //     Serial.print(sensorVal[i]); Serial.print(" ");
  //   } Serial.println(" ");
  // }

  // float calib[6];

  // calib[0] = ( 5.466 + sqrt( pow( -5.466, 2 ) - 4 * 4.6249 * ( -12.818 - sensorVal[0] ))) / ( 2 * 4.6249 );
  // calib[1] = ( sensorVal[1] + 67.364 ) / 51.71;
  // calib[2] = ( 3.3367 + sqrt( pow( -3.3367, 2 ) - 4 * 3.8821 * ( -16.136 - sensorVal[2] ))) / ( 2 * 3.8821 );
  // calib[3] = ( sensorVal[3] + 9.6818 ) / 47.028;

  //  sensorVal[1] = (int)float((sensorVal[1] - 29) / 301 * 147);
  //  sensorVal[2] = (int)float((sensorVal[2] - 11) / 200 * 147);
  //  sensorVal[3] = (int)float((sensorVal[3] - 96) / 269 * 147);
  //  sensorVal[4] = (int)float((sensorVal[4] - 30) / 250 * 147);
  //  sensorVal[5] = sensorVal[5];

  //  //
  //  for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++) {
  //    if (mux.hasChanged(sensor)) {
  //      // 1: wheel modulation ~
  //      controlChange(sensor, 1, sensorVal[sensor]);
  //    }
  //  }
  //
  //  MidiUSB.flush();

  // //
  // if (DEBUG == 1)
  // {
  //   //
  //   int sum = 0;

  //   for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++)
  //     sum += sensorVal[sensor];

  //   //
  //   int average = 0;

  //   average = sum / HOW_MANY_SENSORS;

  //   //
  //   uint8_t lowest = 0;

  //   for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++)
  //     if (sensorVal[sensor] < sensorVal[lowest]) lowest = sensor;

  //   //
  //   uint8_t highest = 0;

  //   for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++)
  //     if (sensorVal[sensor] > sensorVal[highest]) highest = sensor;

  //   delay(10);

  //   Serial.print("sensors values : ");

  //   for (uint8_t sensor = 0; sensor < HOW_MANY_SENSORS; sensor++) {
  //     Serial.print(sensorVal[sensor]); Serial.print(" ");
  //   }

  //   Serial.print("| sum is : ");
  //   Serial.print(sum);

  //   Serial.print(" | average is : ");
  //   Serial.print(average);

  //   Serial.print(" | lowest is : ");
  //   Serial.print(lowest);
  //   Serial.print(" (");
  //   Serial.print(sensorVal[lowest]);
  //   Serial.print(")");

  //   Serial.print(" | highest is : ");
  //   Serial.print(highest);
  //   Serial.print(" (");
  //   Serial.print(sensorVal[highest]);
  //   Serial.print(")");

  //   Serial.println(" ");
  // }
}
