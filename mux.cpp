/*!
    @file       mux.cpp
    @project    Hal-2000
    @brief      Part of the Hal-2000 scenography design, this is a MIDI-based
                controller designed to be a physical interface of Ableton, Millumin, etc.
                Produced by https://www.domes-studio.com/
    @version    0.1.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       01/03/2024

    Copyright (C) 2024 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "mux.hpp"

Mux::Mux(uint8_t how_many_sensors)
{
  _how_many_sensors = how_many_sensors;

  _analogValues = new int[_how_many_sensors];
  _lastAnalogValues = new int[_how_many_sensors];

  _responsive = new ResponsiveAnalogRead*[_how_many_sensors];

  for (uint8_t i = 0; i < _how_many_sensors; i++) {
    _responsive[i] = new ResponsiveAnalogRead(0, true);
    _responsive[i]->enableEdgeSnap();
    _responsive[i]->setAnalogResolution(1023);

    _responsive[i]->update(0);
  }

  if (DEBUG_MUX__ > 0)
  {
    Serial.print("how many sensors ? "); Serial.println((uint8_t)_how_many_sensors);
  }
}

void Mux::setup(const uint8_t analogPin, const uint8_t *pins)
{
  _how_many_pins = (uint8_t) round((double) log(_how_many_sensors) / log(2));
  
  _pins = new uint8_t[_how_many_pins];
  _state = new uint8_t[_how_many_pins];

  if (DEBUG_MUX__ > 0)
  {
    Serial.println("DEBUG MUX SETUP");
    Serial.println("--");
    
    Serial.print("how many sensors ? "); Serial.println(_how_many_sensors);
    Serial.print("how many mux-pins ? "); Serial.println(_how_many_pins);
  }

  for (uint8_t i = 0; i < _how_many_pins; i++)
  {
    _pins[i] = pins[i];
    pinMode(_pins[i], OUTPUT);

    if (DEBUG_MUX__ > 0)
    {
      Serial.print("pin "); Serial.print(i); Serial.print(" : "); Serial.println((uint8_t)_pins[i]);
    }
  }

  _analogPin = analogPin;

  pinMode(_analogPin, INPUT);

  if (DEBUG_MUX__ > 0)
  {
    Serial.print("& analog pin is ");
    Serial.println((uint8_t)_analogPin);
  }

  for (uint8_t sensors = 0; sensors < _how_many_sensors; sensors++)
  {
    // _analogValues[sensors] = 0;
    // _lastAnalogValues[sensors] = 0;

    this->update();
  }
}

void Mux::update()
{
  for (uint8_t sensor = 0; sensor < _how_many_sensors; sensor++)
  {
    // Set s0,s1,s2 pins of 4051-Mux
    for (uint8_t pin = 0; pin < _how_many_pins; pin++) {
      _state[pin] = bitRead(sensor, pin);
      digitalWrite(_pins[pin], _state[pin]);
    }

    int16_t  _value = this->getRaw();

    _responsive[sensor]->update(_value);

    _lastAnalogValues[sensor] = _analogValues[sensor];
    _analogValues[sensor] = _responsive[sensor]->getValue();
  }
}

void Mux::update(uint8_t sensor)
{
  // Set s0,s1,s2 pins of 4051-Mux
  for (uint8_t pin = 0; pin < _how_many_pins; pin++) {
    _state[pin] = bitRead(sensor, pin);
    digitalWrite(_pins[pin], _state[pin]);
  }

  int16_t  _value = this->getRaw();

  _responsive[sensor]->update(_value);

  _lastAnalogValues[sensor] = _analogValues[sensor];
  _analogValues[sensor] = _responsive[sensor]->getValue();
}

void Mux::set(uint8_t sensor)
{
  for (uint8_t i = 0; i < _how_many_pins; i++)
  {
    _state[i] = bitRead(sensor, i);
    digitalWrite(_pins[i], _state[i]);
  }
}

bool Mux::hasChanged(uint8_t sensor)
{
  return _responsive[sensor]->hasChanged();
}

int *Mux::get()
{
  return _analogValues;
}

//
int Mux::get(uint8_t sensor)
{
  return _analogValues[sensor];
}

//
int Mux::getLast(uint8_t sensor)
{
  return _lastAnalogValues[sensor];
}

int Mux::getRaw(void)
{
  //int16_t  _value = (int16_t) ((float) analogRead(_analogPin) / 540 * 127);
  int16_t  _value = analogRead(_analogPin);

  return _value;
}
