#ifndef SENSORS_HPP__
#define SENSORS_HPP__

// //
// const uint16_t sensors_ref[][12] = {  {  0,   0,   9,  30,  78,  82, 182, 183, 348, 489, 504, 521 },
//                                       { 15,  19,  29, 161, 221, 230, 303, 328, 398, 492, 507, 522 },
//                                       {  1,   3,  31,  61, 108, 117, 172, 213, 358, 495, 509, 522 },
//                                       { 41,  54, 153, 185, 248, 259, 313, 340, 409, 502, 517, 531 },
//                                       {  6,  15,  10,  81, 130, 160, 222, 281, 391, 495, 510, 523 },
//                                       {  0,   1,   4,  26,  63, 147, 116, 185, 369, 498, 511, 526 }  };

//
enum jumpDir { inc, dec };

const uint8_t jump[][6] = {  {   8,  8,  8,  8,  8,  8 },
                             {  10, 10, 10, 10, 10, 10 }  };
                                


#endif /* SENSORS */