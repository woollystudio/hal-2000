# Hal-2000

Part of the Hal-2000 scenography design, this is a MIDI-based controller designed to be a physical interface of Ableton, Millumin, etc.
Produced by https://www.domes-studio.com/

![banner-picture](/media/banner.png)

## Dependencies

### Arduino IDE

In the bar menu, click tools > manage libraries, then search and install the ResponsiveAnalogRead library from Damien Clarke. No other library needed.

### Puredata

Example full made in Pd-vanilla, no library install is needed. To launch the example, just open the example.pd patch in Puredata. This patch was made with Pd-0.54-1.
Configure your audio & midi devices in the bar menu, click media > audio | midi parameter ; the Hal 2000 MIDI device name is "Arduino micro".

## Calibration

There is already a pseudo algorithm for sensors detections which can be tuned in the sensor.hpp file :

```cpp
const uint8_t jump[][6] = {  {   8,  8,  8,  8,  8,  8 },
                             {  10, 10, 10, 10, 10, 10 }  };
```

and in the hal-2000.hpp file :

```cpp
#define UPDATE_FREQ 25
```

The jump parameter corresponds to the rising and falling edge for each sensor, while the UPDATE_FREQ parameter corresponds to the frequency (ms) at which the measurements are made.
Here, by default, that means there is a positive detection when sensors measurements were increased by 8 in 25ms, and a negative detection when sensors measurements were decreased by 10 in 25ms. Because each sensor has is own response curve, you can change these paremeters to fine-tune each of them for your needs.
You can find [here](/media/hal-2000-releves-etalonnage.pdf) an empiric measuremnt of each sensors response curve.

## MIDI chart

```plain
MIDI-CC   | channel 1-6 | controller 3 | value 0-127     >>> raw sensor value for each sensor
MIDI-NOTE | channel 1   | note 1-6     | velocity 0-127  >>> pseudo detection value (0: negative, 127: positive)
```